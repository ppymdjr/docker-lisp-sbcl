FROM debian:stretch
WORKDIR /app
ADD ./startup.lisp /app/startup.lisp
RUN cd /app; \
    apt-get -y update; \
    apt-get -y upgrade; \
    apt-get -y install sbcl wget shadowsocks-libev gcc; \
    cd /app; wget https://beta.quicklisp.org/quicklisp.lisp; \
    sbcl --load quicklisp.lisp --eval "(quicklisp-quickstart:install)" --eval "(ql:quickload :swank)" --eval "(sb-ext:quit)"
    
    
EXPOSE 4015
CMD sbcl --load startup.lisp

