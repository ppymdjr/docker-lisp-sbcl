
;; Load quicklisp - didn't seem to work via init
;; (though possibly because I missed a dash in --eval)
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload :swank)
(let ((swank::*loopback-interface* "0.0.0.0"))
  (swank:create-server :port 4015 :dont-close t))

(format t "Running...~%")
